package com.rizvn;

import org.junit.Test;

/**
 * Created by Riz
 */
public class BulkLoaderTest {
  @Test
  public void createWarehouse(){
    new BulkLoader().createWarehouse();
  }

  @Test
  public void deleteWarehouse(){
    new BulkLoader().deleteWarehouse();
  }

  @Test
  public void generateBulkFiles(){
    BulkLoader bulkLoader = new BulkLoader();
    bulkLoader.buildBulk("/opt/Development/dataflows/sanitised", "/opt/Development/dataflows/bulk", 50, "warehouse", "flow");
  }

  @Test
  public void bulkInsert(){
    BulkLoader bulkLoader = new BulkLoader();
    bulkLoader.indexBulk("http://localhost:9200/_bulk", "/opt/Development/dataflows/bulk");
  }
}