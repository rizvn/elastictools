package com.rizvn;

import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import org.apache.commons.io.IOUtils;
import org.apache.http.client.fluent.Request;

import java.io.*;
import java.nio.charset.StandardCharsets;

/**
 * Created by Riz
 */
public class BulkLoader {

  public void buildBulk(final String srcDir, String destDir, Integer bulkSize, String index, String type){
    File sourceDir = new File(srcDir);
    String[] fileNames = sourceDir.list();

    StringBuilder bulkFile = new StringBuilder();
    int count = 0;

    for(final String filename: fileNames){
      //exclude non json files
      if(!filename.endsWith("json")) continue;
      try {
        try(InputStream is = new BufferedInputStream(new FileInputStream(new File(sourceDir, filename).getAbsolutePath()))) {
          String fileContent = IOUtils.toString(is, StandardCharsets.UTF_8).trim();
          bulkFile.append(String.format("{ \"index\": { \"_index\": \"%s\", \"_type\": \"%s\", \"_id\": %s}}\n", index, type, filename.replace(".json", "")));
          bulkFile.append(fileContent.replace("\n", ""));
          bulkFile.append("\n");

          if(++count % bulkSize == 0){
            try (Writer writer = new BufferedWriter(new FileWriter(new File(destDir, "bulk_" + count/bulkSize +".json")))) {
              IOUtils.write(bulkFile.toString().getBytes(), writer, StandardCharsets.UTF_8);
              bulkFile = new StringBuilder();
            }
          }
        }
        catch (Exception ex) {
          throw new IllegalStateException("Failed to bulk: "+ filename , ex);
        }
      }
      catch (Exception ex){
        throw new IllegalStateException(ex);
      }
    }

    //if there flows left to right, because we didnt reach the bundle size then right them now
    if(bulkFile.length() != 0){
      try (Writer writer = new BufferedWriter(new FileWriter(new File(destDir, "bulk_" + (count/bulkSize)+1 +".json")))) {
        IOUtils.write(bulkFile.toString().getBytes(), writer, StandardCharsets.UTF_8);
      }
      catch (Exception ex){
        throw new IllegalStateException(ex);
      }
    }
  }

  public JsonNode deleteWarehouse(){
    try {
      return Unirest.delete("http://localhost:9200/warehouse")
      .asJson().getBody();
    }
    catch (Exception ex){
      throw new IllegalStateException(ex);
    }
  }


  public void createWarehouse(){
    try {
      Request.Put("http://localhost:9200/warehouse")
        .bodyByteArray("{\"mappings\": { \"flow\": { \"dynamic_templates\": [{ \"nested\": { match_mapping_type\": \"object\", \"mapping\": { \"type\": \"nested\" }}}]}}}".getBytes())
        .execute();
    }
    catch (Exception ex){
      throw new IllegalStateException(ex);
    }

  }

  public void indexBulk(String bulkEndpoint, String bulkFilesDir){
    try {
      File sourceDir = new File(bulkFilesDir);
      String[] fileNames = sourceDir.list();

      for(final String filename: fileNames){
        //exclude non json files
        if(!filename.endsWith("json")) continue;
        try {
          try(InputStream is = new BufferedInputStream(new FileInputStream(new File(sourceDir, filename).getAbsolutePath()))) {
            String bulkContent = IOUtils.toString(is, StandardCharsets.UTF_8).trim();
            String result =  Unirest.post(bulkEndpoint)
                                    .body(bulkContent)
                                    .asString()
                                    .getBody();
            System.out.println(result);
          }
          catch (Exception ex) {
            throw new IllegalStateException("Failed to bulk: "+ filename , ex);
          }
        }
        catch (Exception ex){
          throw new IllegalStateException(ex);
        }
      }

    }
    catch (Exception ex){
      throw  new IllegalStateException(ex);
    }
  }
}
